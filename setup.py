setup(
    name='first_project',
    version='0.0.0',
    description='This is my first project',
    author='MarMi',
    author_email='mikhailycheva.mv@phystech.edu',
    url="https://git.sciprog.center/marmi/advanced-python-homework-2023-marmi.git",
    license="CPOL",
    packages=find_packages(include=['controls']),
    )
