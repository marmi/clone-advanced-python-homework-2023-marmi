from unittest import TestCase

from equipment.turtle_device import TurtleDevice


class TurtleDeviceTest(TestCase):

    def setUp(self) -> None:
        self.device = TurtleDevice()

    def test_open(self):
        self.device.open()
        self.device.close()