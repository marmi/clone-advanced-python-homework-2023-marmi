import time
from turtle import Turtle
from equipment.device import SynchronyDevice
from equipment.device import Device
import inspect
from typing import Optional, Collection, Any

class TurtleDevice(SynchronyDevice):
    def open(self):
        super().open()
        self.turtle = Turtle()

    def close (self):
        super().close()
        del self.turtle
        
    @property
    def trait_descriptors(self):
        l = list()
        for i in inspect.getmembers(TurtleDevice):
            if not (i[0].startswith("__") or i[0].startswith("_")) and not inspect.isfunction(i[1]):
                l.append(i[0])
        return l

    @property
    def action_descriptors(self):
        l = list()
        #print(inspect.getmembers(TurtleDevice))
        #for i in dir(TurtleDevice):
        #    # print(type(getattr(TurtleDevice(), i))=='method')
        #    # if not i.startswith("__") and type(getattr(TurtleDevice(), i)) == types.MethodType:
        #    if not i.startswith("__") and inspect.isroutine(inspect.getattr_static(TurtleDevice, i)):
        #        sig = inspect.signature(getattr(TurtleDevice, i))
        #        l.append(i + " " + str(sig))
        #return l
        for i in inspect.getmembers(TurtleDevice):
            # print(type(getattr(TurtleDevice, i)))
            # print(type(getattr(TurtleDevice(), i))=='method')
            if not (i[0].startswith("__") or i[0].startswith("_")) and inspect.isfunction(i[1]):
                sig = inspect.signature(getattr(TurtleDevice, i[0]))
                l.append(i[0]+str(sig))
        return l

    def execute(self, action_name: str, *args, **kwargs):
        """Execute action `action_name`, using `args` and `kwargs` as action argument."""
        if self.state == 1:
            f = getattr(self.turtle, action_name)
            f(int(*args), **kwargs)
        else:
            print("It's not opened")
            self.open()
            #time.sleep(30)
            f = getattr(self.turtle, action_name)
            f(int(*args), **kwargs)

    def read(self, trait_name: str):
        """Read physical state of trait `trait_name` from device."""
        return getattr(self.turtle, trait_name)

    def write(self, trait_name: str, value: Any) -> bool:
        """Pass `value` to trait `trait_name` of device."""
        setattr(self.turtle, trait_name, value)

    def invalidate(self, trait_name: str):
        """Invalidate logical state of trait `trait_name`"""
        if self.read(trait_name) == self.__getitem__(trait_name):
            print("Everything's okey")
        else:
            print(f"Something went wrong with {trait_name}.")

    def __getitem__(self, trait_name: str) -> Optional[Any]:
        """Return logical state of trait `trait_name`."""
        return getattr(self, trait_name)

#print(inspect.getmembers(Turtle))
#t = TurtleDevice()
#print(t.execute("forward", 3))
