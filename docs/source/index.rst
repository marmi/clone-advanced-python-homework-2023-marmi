.. scada documentation master file, created by
   sphinx-quickstart on Thu Sep 28 15:37:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to scada's documentation!
=================================
**Scada** is my project.  It offers a *simple* and
*intuitive* interface.

.. note::

   This project is under active development.

you can use the package ``controls``:

.. automodule: controls/__init__.py
   :members:


.. toctree::
   :maxdepth: 2
   :caption: Contents:




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
